# Projet Matrice de LED sous Arduino

Ce projet présente la création d'un code arduino permettant l'utilisation d'une matrice de 256 LED (16x16).
Il utilise le logiciel MatrixLEDStudio https://github.com/MaximumOctopus/LEDMatrixStudio

**Attention**
Il faut en premier **alimenter la matrice de LED** en 5v (chargeur ou port USB) puis brancher la carte Arduino, sinon la carte ne pouvant alimenter la matrice, elle risque d'être détruite.

 ![Screenshot](PhotoExemple2.jpg)
