//Code à utiliser avec le projet https://github.com/MaximumOctopus/LEDMatrixStudio
// Il faut réaliser les différentes frames avec le logiciel, les exporter et enregistrer le fichier produit avec pour nom img.h à l'emplacement du fichier .ino
//Exemple téléchargable à l'adresse : 
// ===============================================
// LED Matrix Studio "NeoPixel" template
//
// maximumoctopus.com
// maximumoctopus.com/electronics/builder.htm
//
// based on the Adafruit Neopixel demo code
// ===============================================
// 256 <- should say 256

#include <Adafruit_NeoPixel.h>
#include "img.h"

#define PIN 6 /définition du port de sortie

// Parameter 1 = number of pixels in strip
// Parameter 2 = pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(256, PIN, NEO_GRB + NEO_KHZ800);

void setup() {
  strip.begin();
  strip.setBrightness(20);
  strip.show();   // Initialise tous les pixels à 'éteint'

  RenderFrame(0);
}

void loop() {
//delay(1000);
 // on doit modifier ici le nombre d'images au total i<4 pour 5 images 0 1 2 3 4
  for (int i = 0; i < 4; i++) {
    RenderFrame(i);
    delay(1000);
  }
}

void RenderFrame(int frameIndex) {
  const uint32_t* ledarray;
  
  switch (frameIndex) {
    case 0:
      ledarray = ledarray0;
      break;
    case 1:
      ledarray = ledarray1;
      break;
    case 2:
      ledarray = ledarray2;
      break;
    case 3:
      ledarray = ledarray3;
      break;
    case 4:
      ledarray = ledarray4;
      break;
    // case 5:
    //   ledarray = ledarray5;
    //   break;
    // case 6:
    //   ledarray = ledarray6;
    //   break;  
    // case 7:
    //   ledarray = ledarray7;
    //   break;
    // case 8:
    //   ledarray = ledarray8;
    //   break;  
    // case 9:
    //   ledarray = ledarray9;
    //   break;     
  // Le code est prévu pour 10 images différentes, on devrat commenter/décommenter ou en rajouter si nécessaire.
  // Il en faut au minimum un nombre égale au nombre d'image à afficher dans le void loop.

  }
  
  for (int t = 0; t < 256; t++) {
    strip.setPixelColor(t, pgm_read_dword(&(ledarray[t])));
  }
  
  strip.show();
}
